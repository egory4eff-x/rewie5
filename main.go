package main

import (
	"fmt"
	"github.com/streadway/amqp"
	ordertracking "gitlab.com/egory4eff-x/rewie5/order"
	"google.golang.org/protobuf/proto"
	"log"
)

func main() {
	fmt.Println("Welcome to RabbitMQ")
	// Инициализация RabbitMQ
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		log.Fatalf("Failed to connect to RabbitMQ: %v", err)
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		log.Fatalf("Failed to open a channel: %v", err)
	}
	defer ch.Close()

	// Создание очереди
	q, err := ch.QueueDeclare(
		"order_queue",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf("Failed to declare a queue: %v", err)
	}

	// Получение заказов из очереди и обработка
	msgs, err := ch.Consume(
		q.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf("Failed to register a consumer: %v", err)
	}

	for msg := range msgs {
		order := &ordertracking.Order{}
		err := proto.Unmarshal(msg.Body, order)
		if err != nil {
			log.Printf("Failed to unmarshal order: %v", err)
			continue
		}

		// Обработка заказа и отправка статуса
		status := &ordertracking.OrderStatus{
			OrderId: order.OrderId,
			Status:  "Order processed successfully",
		}

		statusBytes, err := proto.Marshal(status)
		if err != nil {
			log.Printf("Failed to marshal status: %v", err)
			continue
		}

		err = ch.Publish(
			"",
			"status_queue",
			false,
			false,
			amqp.Publishing{
				ContentType: "application/octet-stream",
				Body:        statusBytes,
			})
		if err != nil {
			log.Printf("Failed to publish status: %v", err)
		}
	}
}
