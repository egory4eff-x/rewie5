package main

import (
	"github.com/streadway/amqp"
	ordertracking "gitlab.com/egory4eff-x/rewie5/order"
	"google.golang.org/protobuf/proto"
	"log"
)

func main() {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		log.Fatalf("Failed to connect to RabbitMQ: %v", err)
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		log.Fatalf("Failed to open a channel: %v", err)
	}
	defer ch.Close()

	// Отправка заказа
	order := &ordertracking.Order{
		OrderId:     1,
		ProductName: "Product",
		Quantity:    10,
		TotalPrice:  100.0,
		Client:      123,
	}

	orderBytes, err := proto.Marshal(order)
	if err != nil {
		log.Fatalf("Failed to marshal order: %v", err)
	}

	err = ch.Publish(
		"",
		"order_queue",
		false,
		false,
		amqp.Publishing{
			ContentType: "application/octet-stream",
			Body:        orderBytes,
		})
	if err != nil {
		log.Fatalf("Failed to publish order: %v", err)
	}

	// Получение статуса
	statusQueue, err := ch.QueueDeclare(
		"status_queue",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf("Failed to declare status queue: %v", err)
	}

	msgs, err := ch.Consume(
		statusQueue.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf("Failed to register a consumer: %v", err)
	}

	for msg := range msgs {
		status := &ordertracking.OrderStatus{}
		err := proto.Unmarshal(msg.Body, status)
		if err != nil {
			log.Printf("Failed to unmarshal status: %v", err)
			continue
		}

		log.Printf("Received status: OrderID %d, Status: %s", status.OrderId, status.Status)
	}
}
